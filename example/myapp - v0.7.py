#coding:utf-8

#sae flask 微信公众平台开发代码 
#1.实现发送任意信息自动回复 2015.9.29
#
#5.实现决断接收的类型
#6.实现菜单响应功能,注这是在接口测试账号中测试
#7.把xml信息文本分离

from flask import Flask, request, make_response
import hashlib
import xml.etree.ElementTree as ET
import time
import xml_config

app = Flask(__name__)
app.debug = True

@app.route('/', methods=['GET', 'POST'])
def wechat_auth():
    if request.method == 'GET':
        return checkSignature(request)
    else:
        return responseMsg(request)

def checkSignature(request):
    token = 'weixin'
    signature = request.args.get('signature', '')
    timestamp = request.args.get('timestamp', '')
    nonce = request.args.get('nonce', '')
    echoStr = request.args.get('echostr', '')
    tmpStr = ''.join(sorted([token, timestamp, nonce]))
    if hashlib.sha1(tmpStr).hexdigest() == signature:
        return make_response(echoStr)

def responseMsg(request):
    rec = request.stream.read()
    xml_rec = ET.fromstring(rec)
    msgtype = xml_rec.find('MsgType').text
    toUsername = xml_rec.find('ToUserName').text
    fromUsername = xml_rec.find('FromUserName').text
    if msgtype == "event":
        msgcontent = xml_rec.find('Event').text
        if msgcontent == "subscribe":
            msgcontent = "欢迎关注 idong"
            # response = make_response(xml_rep_img % (fromUsername, toUsername, str(int(time.time()))))
            response = make_response(xml_config.text % (fromUsername, toUsername, str(int(time.time())), msgcontent))
        elif msgcontent == "CLICK":
            eventKey = xml_rec.find('EventKey').text
            if eventKey == "V1001_TODAY_MUSIC":
                msgImage = u'您点击了今日歌曲'
                response = make_response(xml_config.text % (fromUsername, toUsername, str(int(time.time())), msgImage))
            elif eventKey == "V1001_GOOD":
                msgTitle = 'DONGBLOG'
                msgDescription = 'python life blog'
                msgPicUrl = 'http://pycodelife-mystorage.stor.sinaapp.com/python.jpg'
                msgUrl = 'http://pycodelife.sinaapp.com/'
                response = make_response(xml_config.xml_rep_img % (fromUsername, toUsername, str(int(time.time())), msgTitle, msgDescription, msgPicUrl, msgUrl))
    elif msgtype == "text":
        content = xml_rec.find('Content').text
        #这里是关键字，如果要使用中文关键字要加上u
        if content == u'博客':
            msgTitle = 'DONGBLOG'
            msgDescription = 'python life blog'
            msgPicUrl = 'http://pycodelife-mystorage.stor.sinaapp.com/python.jpg'
            msgUrl = 'http://pycodelife.sinaapp.com/'
            response = make_response(xml_config.xml_rep_img % (fromUsername, toUsername, str(int(time.time())), msgTitle, msgDescription, msgPicUrl, msgUrl))
        else:
            response = make_response(xml_config.text % (fromUsername, toUsername, str(int(time.time())), content))
    elif msgtype == "image":
        msgImage = u'您发送了一张图片'
        response = make_response(xml_config.text % (fromUsername, toUsername, str(int(time.time())), msgImage))
    response.content_type='application/xml'
    return response