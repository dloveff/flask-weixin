from flask import Flask, request, make_response
import hashlib

app = Flask(__name__)
app.debug = True

@app.route('/', methods=['GET', 'POST'])
def wechat_auth():
    if request.method == 'GET':
        return checkSignature(request)
    else:
        return None

def checkSignature(request):
    token = 'weixin'
    signature = request.args.get('signature', '')
    timestamp = request.args.get('timestamp', '')
    nonce = request.args.get('nonce', '')
    echoStr = request.args.get('echostr', '')
    tmpStr = ''.join(sorted([token, timestamp, nonce]))
    if hashlib.sha1(tmpStr).hexdigest() == signature:
        return make_response(echoStr)