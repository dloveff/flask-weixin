# -*- encoding: utf-8 -*-

import urllib
import urllib2
from urllib import urlencode
import json
import sys
reload(sys)
sys.setdefaultencoding('UTF-8')

appid = 'wx1c90df2cce19a9a4'
secret = '974a308e03d756ac2b6cc9a9699e6d4d'

gettoken = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appid + '&secret=' + secret

f = urllib2.urlopen( gettoken )


stringjson = f.read()

access_token = json.loads(stringjson)['access_token']

print access_token

# 创建菜单
posturl = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + access_token
# 查询菜单
getMenu = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + access_token
# 删除菜单
deleteMenu = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + access_token

menu = '''{
    "button": [
        {
            "name": "账户", 
            "sub_button": [
                {
                    "type": "click", 
                    "name": "绑定账户", 
                    "key": "binding"
                }, 
                {
                    "type": "click", 
                    "name": "解除绑定", 
                    "key": "unlock"
                }
            ]
        }, 
        {
            "name": "校园大杂烩", 
            "sub_button": [
                {
                    "type": "click", 
                    "name": "近期成绩", 
                    "key": "grade"
                }
            ]
        }, 
        {
            "name": "大杂烩②", 
            "sub_button": [
                {
                    "type": "click", 
                    "name": "使用手册", 
                    "key": "userguide"
                }
            ]
        }
    ]
}'''

# request = urllib2.urlopen(posturl, menu.encode('utf-8'))
request = urllib2.urlopen(getMenu)
# request = urllib2.urlopen(deleteMenu)

print request.read()