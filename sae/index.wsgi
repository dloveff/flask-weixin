import sae
from myapp import app
import os
import sys

# Loading requests
# kennethreitz-requests-v2.7.0-110-gb8df891
root = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(root, 'kennethreitz-requests-v2.7.0-110-gb8df891'))


application = sae.create_wsgi_app(app)